__author__ = 'azarux'

from gen_programming import *
from gp_utils import *

from random import randint, random
from copy import deepcopy
import operator
import time


genetic_bank = [
    GenInfo(function=operator.add,
            rtype='Int',
            arg_types=['Int', 'Int'],
            name='Add'),

    GenInfo(function=operator.sub,
            rtype='Int',
            arg_types=['Int', 'Int'],
            name='Sub'),

    GenInfo(function=operator.mul,
            rtype='Int',
            arg_types=['Int', 'Int'],
            name='Mul'),
    
    GenInfo(function=operator.floordiv,
            rtype='Int',
            arg_types=['Int', 'Int'],
            name='Div'),
    
    GenInfo(function=operator.mod,
            rtype='Int',
            arg_types=['Int', 'Int'],
            name='Mod'),

    GenInfo(function=lambda x: x,
            rtype='Int',
            arg_types=['Int'],
            name='Id'),
            
    GenInfo(function=if_func,
            rtype='Int',
            arg_types=['Bool', 'Int', 'Int'],
            name='If'),
            
    GenInfo(function=if_filter,
            rtype='Int',
            arg_types=['Bool', 'Int'],
            name='Iff'),
            
    GenInfo(function=operator.lt,
            rtype='Bool',
            arg_types=['Int', 'Int'],
            name='Lt'),
            
    GenInfo(function=operator.le,
            rtype='Bool',
            arg_types=['Int', 'Int'],
            name='Le'),
            
    GenInfo(function=operator.eq,
            rtype='Bool',
            arg_types=['Int', 'Int'],
            name='Eq'),
            
    GenInfo(function=operator.ne,
            rtype='Bool',
            arg_types=['Int', 'Int'],
            name='Ne'),
            
    GenInfo(function=operator.ge,
            rtype='Bool',
            arg_types=['Int', 'Int'],
            name='Ge'),
            
    GenInfo(function=operator.gt,
            rtype='Bool',
            arg_types=['Int', 'Int'],
            name='Gt'),

    GenInfo(function=operator.and_,
            rtype='Bool',
            arg_types=['Bool', 'Bool'],
            name='And'),

    GenInfo(function=operator.or_,
            rtype='Bool',
            arg_types=['Bool', 'Bool'],
            name='Or'),

    GenInfo(function=is_prime,
            rtype='Bool',
            arg_types=['Int'],
            name='IsPrime'),

    GenInfo(function=lambda n: n % 2 == 0,
            rtype='Bool',
            arg_types=['Int'],
            name='IsEven'),

    GenInfo(function=lambda n: n % 2 != 0,
            rtype='Bool',
            arg_types=['Int'],
            name='IsOdd'),
            
    GenInfo(function=sum,
            rtype='Int',
            arg_types=['[Int]'],
            name='Sum'),
            
    GenInfo(function=product,
            rtype='Int',
            arg_types=['[Int]'],
            name='Product'),
            
    GenInfo(function=len,
            rtype='Int',
            arg_types=['[Int]'],
            name='Length'),
            
    GenInfo(function=take_last_n,  # take last n elements
            rtype='[Int]',
            arg_types=['[Int]', 'Int'],
            name='LastN'),
            
    GenInfo(function=factors,
            rtype='[Int]',
            arg_types=['Int'],
            name='Factors')
]


class PredictionGeneticSolver:
    def __init__(self,
                 population_size=10000,
                 max_program_tree_depth=5,
                 p_crossover=0.8,
                 p_mutation=0.2,
                 max_iterations_number=1000,
                 min_random_number=-5,
                 max_random_number=5,
                 p_current_number=0.9,
                 max_execution_time=0.05):
        self.gp_environment = GPEnvironment(
            population_size, max_program_tree_depth,
            p_crossover, p_mutation, max_iterations_number)
        
        self.functions_dict = dict()
        for gen_info in genetic_bank:
            if gen_info.rtype in self.functions_dict:
                self.functions_dict[gen_info.rtype].append(gen_info)
            else:
                self.functions_dict[gen_info.rtype] = [gen_info]
                
        self.p_current_number = p_current_number
        self.min_random_number = min_random_number
        self.max_random_number = max_random_number
        self.max_execution_time = max_execution_time
        
        self.predictions_count = 0
        self.current_sequence = []
        self.current_answers = []
        self.current_number = 0
        self.previous_elements = []
        
    def predict(self, xs, answers):
        self.predictions_count = len(answers)
        self.current_answers = answers
        self.current_sequence = xs

        model = self.gp_environment.find_solution('Int', self, self.fitness_function)
        if model is None:
            return []
        return self._predict(model), model
        
    def _predict(self, model):
        self.previous_elements = deepcopy(self.current_sequence)

        ts = time.time()
        predictions = []
        self.current_number = self.current_sequence[-1] + 1
        while len(predictions) != self.predictions_count:
            prediction = model.run()

            if not (prediction is None):
                predictions.append(prediction)
                self.previous_elements.append(prediction)
            self.current_number += 1

            elapsed = time.time() - ts
            if elapsed > self.max_execution_time:
                predictions = []
                break

        return predictions      
        
    def get_function(self, rtype):
        possible_functions = self.functions_dict[rtype]
        f = choice(possible_functions)
        return f
        
    def get_terminal(self, rtype):
        if rtype == 'Int':
            terminal_function, name = self.get_int_terminal()
        elif rtype == '[Int]':
            terminal_function, name = self.get_list_terminal()
        elif rtype == 'Bool':
            terminal_function, name = self.get_bool_terminal()
        else:
            raise NameError(rtype)
        return GenInfo(terminal_function, rtype, [], name)
            
    def get_int_terminal(self):
        if random() < self.p_current_number:
            return self.get_current_number()
        else:
            return self.get_random_number()
            
    def get_list_terminal(self):
        return self.get_previous_elements()
        
    def get_bool_terminal(self):  # doesn't make sense
        b = random() < 0.5

        def terminal():
            return b
        return terminal, str(b)
        
    def get_previous_elements(self):
        def terminal():
            return self.previous_elements
        return terminal, 'previous_elements'
        
    def get_random_number(self):
        n = randint(self.min_random_number, self.max_random_number)

        def terminal():
            return n
        return terminal, str(n)
        
    def get_current_number(self):
        def terminal():
            return self.current_number
        return terminal, 'k'

    def fitness_function(self, individual):
        solved = False
        try:
            predictions = self._predict(individual)
            cpl = len(common_prefix(predictions, self.current_answers))
            fitness_value = cpl / (self.predictions_count * len(individual.gen_storage))
            solved = cpl == self.predictions_count
        except:
            fitness_value = 0.0
            
        return fitness_value, solved