__author__ = 'azarux'

from functools import reduce
from random import random, choice
from itertools import takewhile
import operator


def cumsum(xs):
    cs = [0] * len(xs)
    cs[0] = xs[0]
    for i in range(1, len(xs)):
        cs[i] = cs[i - 1] + xs[i]
    return cs


def discrete_sample(ps, xs):
    n = sum(ps)
    if n == 0:
        return choice(xs)
    nps = list(map(lambda p: p / n, ps))
    return next(x for x, c in zip(xs, cumsum(nps)) if random() <= c)
    
    
def common_prefix(xs, ys):
    return list(map(lambda p: p[0], takewhile(lambda p: p[0] == p[1], zip(xs, ys))))


def if_func(cnd, a, b):
    return a if cnd else b


def if_filter(cnd, a):
    return a if cnd else None


def product(xs):
    return reduce(operator.mul, xs, 1)


def take_last_n(xs, n):
    if n > len(xs):
        raise RuntimeError('Invalid n!')
    return xs[len(xs)-n:]


def factors(n):
    if n > 10000:
        raise RuntimeError('Im dumb!')
    return list(f for f in range(1, n + 1) if n % f == 0)


def is_prime(n):
    return factors(n) == [1, n]