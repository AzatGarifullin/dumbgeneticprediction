__author__ = 'azarux'

from gp_prediction import PredictionGeneticSolver
from gp_utils import is_prime


gp_prediction = PredictionGeneticSolver()


def test_predictor(ans, xs):
    predictions, model = gp_prediction.predict(xs, ans)
    if predictions == ans:
        print('Test simple: passed')
    else:
        print('Test simple: failed')
    print('Predictions: ' + str(predictions))
    print('Model: ' + str(model))


def test_simple():
    xs = [1, 2, 3, 4, 5]
    ans = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    test_predictor(ans, xs)


def test_fibs():
    xs = [1, 1, 2, 3, 5]
    ans = [8, 13, 21, 34, 55, 89, 144, 233, 377]
    test_predictor(ans, xs)


def test_primes():
    xs = [1, 2, 3, 5, 7]
    ans = [11, 13, 17, 19, 23, 29, 31, 37, 41, 43]
    test_predictor(ans, xs)


def gen_primes(length):
    num = 1
    primes = [num]
    while len(primes) < length:
        num += 1
        if is_prime(num):
            primes.append(num)
    return primes


def test_every_second_prime():
    primes = gen_primes(30)
    primes = primes[::2]
    xs, ans = primes[0:5], primes[5:15]
    test_predictor(ans, xs)



if __name__ == '__main__':
    # test_simple()
    # test_fibs()
    # test_primes()
    test_every_second_prime()

