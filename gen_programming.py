__author__ = 'azarux'

from random import random
from random import randint
from gp_utils import discrete_sample
import copy


class GenInfo:
    def __init__(self, function=None, rtype=None, arg_types=None, name=''):
        self.function = function
        self.rtype = rtype
        self.arg_types = arg_types
        self.name = name

    def arity(self):
        return len(self.arg_types)


class GenTree:
    def __init__(self, gen_storage, gen_info=None):
        self.gen_info = gen_info
        self.children = []

        self.gen_storage = gen_storage
        self.gen_storage.append(self)
        self.index = len(self.gen_storage) - 1

    @classmethod
    def create_grow(cls, gen_storage, rtype, max_depth, p_getter):
        t_cnd = lambda depth: depth == 1 or random() <= 0.5
        return cls._create(gen_storage, rtype, max_depth, p_getter, t_cnd)

    @classmethod
    def create_full(cls, gen_storage, rtype, max_depth, p_getter):
        t_cnd = lambda depth: depth == 1
        return cls._create(gen_storage, rtype, max_depth, p_getter, t_cnd)

    @classmethod
    def ramped_half_and_half(cls, gen_storage, rtype, max_depth, p_getter):
        if random() < 0.5:
            return cls.create_grow(gen_storage, rtype, max_depth, p_getter)
        else:
            return cls.create_full(gen_storage, rtype, max_depth, p_getter)

    @classmethod
    def _create(cls, gen_storage, rtype, depth, p_getter, term_condition):
        tree = cls(gen_storage)

        if term_condition(depth):
            tree.gen_info = p_getter.get_terminal(rtype)
        else:
            tree.gen_info = p_getter.get_function(rtype)
            create_child = lambda t: \
                cls._create(gen_storage, t, depth - 1, p_getter, term_condition)
            tree.children = list(map(create_child, tree.gen_info.arg_types))

        return tree

    def copy_to(self, other):
        self.gen_info = copy.deepcopy(other.gen_info)
        self.children = copy.deepcopy(other.children)

    def set_storage(self, storage):
        self.gen_storage = storage
        self.gen_storage.append(self)
        self.index = len(self.gen_storage) - 1

        for child in self.children:
            child.set_storage(storage)

    def update_gen_storage(self):
        del self.gen_storage[:]
        self.set_storage(self.gen_storage)

    def crossover(self, other):
        self_crossover_point = randint(1, len(self.gen_storage) - 1)
        compatible_type = lambda n: \
            n.gen_info.rtype == self.gen_storage[self_crossover_point].gen_info.rtype
        other_possible_nodes = list(filter(compatible_type, other.gen_storage))
        if not other_possible_nodes:
            if random() < 0.5:
                return copy.deepcopy(self)
            else:
                return copy.deepcopy(other)
        other_crossover_point = randint(0, len(other_possible_nodes) - 1)

        child = copy.deepcopy(self)
        self_crossover_node = child.gen_storage[self_crossover_point]
        other_crossover_node = other_possible_nodes[other_crossover_point]
        other_crossover_node.copy_to(self_crossover_node)
        child.update_gen_storage()

        return child

    def mutation(self, max_depth, p_getter):
        hc_gen_storage = []
        headless_chicken = GenTree.create_full(
            hc_gen_storage, self.gen_info.rtype, max_depth, p_getter)
        return headless_chicken.crossover(self)

    def run(self):
        args = list(map(lambda c: c.run(), self.children))
        return self.gen_info.function(*args)

    def __str__(self):
        sfx = ''
        if self.children:
            args_str = ''
            for child in self.children:
                args_str = args_str + str(child) + ', '
            sfx = '(' + args_str[:-2] + ')'
        return self.gen_info.name + sfx

    def __repr__(self):
        return str(self)


class ProgramIndividual:
    def __init__(self):
        self.gen_storage = []
        self.fitness_value = 0
        self.tree = None

    @classmethod
    def create(cls, rtype, depth, p_getter):
        individual = cls()
        individual.gen_storage = []
        individual.fitness_value = 0
        individual.tree = GenTree.ramped_half_and_half(
            individual.gen_storage, rtype, depth, p_getter)
        return individual        

    def crossover(self, other):
        if len(self.gen_storage) < 3:
            return copy.deepcopy(other)

        offspring = ProgramIndividual()
        offspring.tree = self.tree.crossover(other)
        offspring.gen_storage = offspring.tree.gen_storage
        return offspring

    def mutation(self, max_depth, p_getter):
        mutant = ProgramIndividual()
        mutant.tree = self.tree.mutation(max_depth, p_getter)
        return mutant

    def run(self):
        return self.tree.run()

    def __str__(self):
        return str(self.tree)

    def __repr__(self):
        return repr(self.tree)


class GPEnvironment:
    def __init__(self,
                 population_size,
                 max_program_tree_depth,
                 p_crossover,
                 p_mutation,
                 max_iterations_number
                 ):
        self.population_size = population_size
        self.max_program_tree_depth = max_program_tree_depth
        self.p_crossover = p_crossover
        self.p_mutation = p_mutation
        self.max_iterations_number = max_iterations_number
        self.population = []

    def find_solution(self, rtype, primitives_getter, fitness_function):
        self.init_population(rtype, primitives_getter, fitness_function)

        iteration = 0
        converged = False
        solution = None
        while not converged and iteration < self.max_iterations_number:
            offsprings = self.perform_crossovers()
            mutants = self.perform_mutations(primitives_getter)
            converged = self.perform_selection(offsprings, mutants, fitness_function)

            iteration += 1
            print('Iteration: ' + str(iteration))

        if solution is None:
            solution = self.population[-1]
        return solution

    def init_population(self, rtype, primitives_getter, fitness_function):
        self.population = []
        for i in range(0, self.population_size):
            individual = ProgramIndividual.create(
                rtype, self.max_program_tree_depth, primitives_getter)
            individual.fitness_value, _ = fitness_function(individual)
            self.population.append(individual)

    def perform_crossovers(self):
        offsprings = []
        ps = list(map(lambda i: i.fitness_value, self.population))
        if sum(ps) == 0:
            ps = [1 / self.population_size] * self.population_size

        for i in range(0, self.population_size):
            individual = self.population[i]
            if random() < self.p_crossover:
                partner = self.find_partner_for(i, ps)
                offsprings.append(individual.crossover(partner))

        return offsprings

    def find_partner_for(self, individual_index, ps):
        p, ps[individual_index] = ps[individual_index], 0
        partner = discrete_sample(ps, self.population)
        ps[individual_index] = p
        return partner

    def perform_mutations(self, p_getter):
        mutants = []
        for individual in self.population:
            if random() < self.p_mutation:
                mutants.append(individual.mutation(
                    self.max_program_tree_depth, p_getter))

        return mutants

    def perform_selection(self, offsprings, mutants, fitness_function):
        converged = False

        self.population = self.population + offsprings + mutants
        for individual in self.population:
            individual.fitness_value, solved = fitness_function(individual)
            converged = converged or solved

        self.population = sorted(self.population, key=lambda i: i.fitness_value)
        self.population = self.population[len(offsprings) + len(mutants):]

        return converged